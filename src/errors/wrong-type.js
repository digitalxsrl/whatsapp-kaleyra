/**
 * Custom Error to handle unsupported message type from Kaleyra
 */
class WhatsappWrongTypeError extends Error {
  
  constructor(wrong_type_message, ...params) {
    
    // Pass remaining arguments
    super(...params);
    
    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, WhatsappWrongTypeError);
    }
    
    this.name = 'WhatsappWrongTypeError';
    
    // Default message
    this.wrong_type_message = wrong_type_message;
  }
  
}

module.exports = WhatsappWrongTypeError;