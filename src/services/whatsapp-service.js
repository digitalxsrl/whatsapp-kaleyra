'use strict'
const fs = require('fs');
const moment = require('moment-timezone');
const request = require('request-promise-native');
const ERRORS = require('../errors');

// setting global timezone
moment.tz.setDefault("Europe/Rome");


const kaleyraDomain = "https://api.kaleyra.io/v1";


/**
 * Send messages through WhatsApp with Kaleyra API - https://developers.kaleyra.io/docs
 */
class WhatsappService {
  
  /**
   * Create an instance of Whatsapp Kaleyra Service
   * @param sid
   * @param apiKey
   * @param acceptedTypes
   */
  constructor(sid, apiKey, waNumber) {
    this.sid = sid;
    this.apiKey = apiKey;
    this.sender = waNumber;
  }
  
  /**
   * Send a simple text message
   * @param toNumber WhatsApp Number to Send
   * @param txtContent Simple text content
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendText(toNumber, txtContent, callbackUrl) {
    let bodyToSend = {
      from: this.sender,
      to: toNumber,
      type: "text",
      channel: "whatsapp",
      body: txtContent,
      callback_url: callbackUrl
    };
    
    let options = {
      uri: `${kaleyraDomain}/${this.sid}/messages`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "api-key": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return request(options);
  }
  
  /**
   * Send a template message approved by Facebook
   * @param toNumber WhatsApp Number to Send
   * @param templateName Template identifier
   * @param params Array of params
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendTextTemplate(toNumber, templateName, params, param_url, callbackUrl) {
    let bodyToSend = {
      from: this.sender,
      to: toNumber,
      type: "template",
      channel: "whatsapp",
      template_name: templateName,
      params: (params || []).map(function (e) {
        e = '"' + e + '"';
        return e;
      }).join(','),
      lang_code: 'it',
      callback_url: callbackUrl,
      param_url: param_url || ""
    };
    
    let options = {
      uri: `${kaleyraDomain}/${this.sid}/messages`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "api-key": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return request(options);
  }

  /**
   * Send a template message with image approved by Facebook
   * @param toNumber WhatsApp Number to Send
   * @param templateName Template identifier
   * @param media_url media_url
   * @param params Array of params
   * @param param_url String of button url
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendMediaTemplate(toNumber, templateName, media_url, params, param_url, callbackUrl) {
    let bodyToSend = {
      from: this.sender,
      to: toNumber,
      type: "mediatemplate",
      channel: "whatsapp",
      template_name: templateName,
      params: (params || []).map(function (e) {
        e = '"' + e + '"';
        return e;
      }).join(','),
      media_url: media_url,
      lang_code: 'it',
      callback_url: callbackUrl,
      param_url: param_url || "",
    };

    let options = {
      uri: `${kaleyraDomain}/${this.sid}/messages`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "api-key": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return request(options);
  }
  
  /**
   * Send an image
   * @param toNumber WhatsApp Number to Send
   * @param imagePath Server path of the image
   * @param imageTitle Image file name with extension; mandatory if @imagePath doesn't contains it
   * @param txtContent Simple text content
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendImage(toNumber, imagePath, imageTitle, txtContent, callbackUrl) {
    let options = {
      uri: `${kaleyraDomain}/${this.sid}/messages`,
      method: 'POST',
      formData: {
        from: this.sender,
        to: toNumber,
        type: "media",
        channel: "whatsapp",
        caption: txtContent, // doesn't work
        media: {
          value: request.get(imagePath), // without await or doesn't work
          options: {
            filename: imageTitle || ''
          }
        },
        callback_url: callbackUrl || ''
      },
      json: true,
      headers: {
        "api-key": this.apiKey,
        "Content-Type": "multipart/form-data"
      }
    };
    return request(options);
  }
  
  /**
   * Send a document
   * @param toNumber WhatsApp Number to Send
   * @param documentPath Document server path
   * @param documentTitle Document file name with extension; mandatory if @documentPath doesn't contains it
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendDocument(toNumber, documentPath, documentTitle, callbackUrl) {
    let options = {
      uri: `${kaleyraDomain}/${this.sid}/messages`,
      method: 'POST',
      formData: {
        from: this.sender,
        to: toNumber,
        type: "media",
        channel: "whatsapp",
        media: {
          value: request.get(documentPath), // without await or doesn't work
          options: {
            filename: documentTitle || '',
          }
        },
        callback_url: callbackUrl || ''
      },
      json: true,
      headers: {
        "api-key": this.apiKey,
        "Content-Type": "multipart/form-data"
      }
    };
    return request(options);
  }
  
  /**
   * Read an incoming message from kaleyra and return an object Message
   * @param kaleyraMsg incoming message
   * @returns {{ds_name: (*|string), media_mime: *, user_third_party_id: *, company_id: *, media_third_party_id: *, cd_type: (*|string), dt_sent: (*|moment.Moment), ds_body: *, ds_phone: *, media_url: (*|string)}|{ds_name: (*|string), media_mime: *, user_third_party_id: *, company_id: *, media_third_party_id: *, cd_type: (*|string), dt_sent: (*|moment.Moment), ds_phone: *, media_filename: string | HTMLElement | BodyInit | ReadableStream<Uint8Array>, media_url: (*|string)}|{ds_name: (*|string), user_third_party_id: *, cd_type: (*|string), dt_sent: (*|moment.Moment), ds_body: *, ds_phone: *}}
   */
  readIncoming(kaleyraMsg) {
    switch (kaleyraMsg.type) {
      case 'text':
        return {
          ds_body: kaleyraMsg.body || '',
          cd_type: kaleyraMsg.type || '',
          user_third_party_id: kaleyraMsg.reply_to,
          dt_sent: moment(kaleyraMsg.created_at, 'X'),
          ds_phone: kaleyraMsg.from,
          ds_name: kaleyraMsg.name || ''
        };
      case 'document':
        return {
          cd_type: kaleyraMsg.type || '',
          user_third_party_id: kaleyraMsg.reply_to,
          dt_sent: moment(kaleyraMsg.created_at, 'X'),
          media_third_party_id: kaleyraMsg.media_name,
          media_mime: kaleyraMsg.mime_type,
          media_filename: kaleyraMsg.body,
          media_url: kaleyraMsg.media_url,
          company_id: kaleyraMsg.comapny_id,
          ds_phone: kaleyraMsg.from,
          ds_name: kaleyraMsg.name || ''
        };
      case 'image':
        return {
          cd_type: kaleyraMsg.type || '',
          media_third_party_id: kaleyraMsg.media_name,
          user_third_party_id: kaleyraMsg.reply_to,
          media_mime: kaleyraMsg.mime_type,
          dt_sent: moment(kaleyraMsg.created_at, 'X'),
          ds_body: kaleyraMsg.body || '',
          media_url: kaleyraMsg.media_url || '',
          company_id: kaleyraMsg.comapny_id,
          ds_phone: kaleyraMsg.from,
          ds_name: kaleyraMsg.name || ''
        };
      default:
        throw new ERRORS.WrongType({
            ds_body: 'Tipo di messaggio non consentito',
            cd_type: 'text',
            user_third_party_id: kaleyraMsg.reply_to,
            dt_sent: moment(kaleyraMsg.created_at, 'X'),
            ds_phone: kaleyraMsg.from,
            ds_name: kaleyraMsg.name || ''
          },
          'Unsupported Type');
    }
  }
  
}

module.exports = WhatsappService;